using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using New_Solution_Template.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using RazorLight;

namespace New_Solution_Template.Services.Email
{
    public class EmailSender : IEmailSender
    {
        private readonly AppSettings _appSettings;
        private readonly ILogger<EmailSender> _logger;
        private readonly RazorLightEngine _razorEngine;

        public EmailSender(AppSettings appSettings,
                           ILogger<EmailSender> logger,
                           IHostingEnvironment env)
        {
            _appSettings = appSettings;
            _logger = logger;
            var templatesPath = env.ContentRootPath + Path.DirectorySeparatorChar.ToString() + "MailTemplates";

            _razorEngine = new RazorLightEngineBuilder()
              .UseFilesystemProject(templatesPath)
              .UseMemoryCachingProvider()
              .Build();
        }
        public async Task SendEmailAsync(string recipient, string subject, string htmlMessage)
        {
            await SendEmailAsync(recipient, "", subject, htmlMessage);
        }
        public async Task SendEmailAsync(string recipient, string reciepentName, string subject, string htmlMessage)
        {
            var _recipient = new MailAddress(recipient, reciepentName, Encoding.UTF8);
            await SendEmailAsync(_recipient, subject, htmlMessage);
        }
        public async Task SendEmailAsync(MailAddress recipient, string subject, string htmlMessage)
        {
            //return Task.CompletedTask;
            var server = _appSettings.Smtp.Server;
            int port = _appSettings.Smtp.Port;
            var username = _appSettings.Smtp.Username;
            var pass = _appSettings.Smtp.Pass;
            var from = _appSettings.Smtp.From;
            var fromName = _appSettings.Smtp.FromName;
            var useSsl = _appSettings.Smtp.UseSsl;

            try
            {
                var sender = new MailboxAddress(fromName, from);

                var email = new MimeMessage();
                email.From.Add(sender);
                email.To.Add(new MailboxAddress(recipient.DisplayName, recipient.Address));

                var builder = new BodyBuilder
                {
                    TextBody = System.Text.RegularExpressions.Regex.Replace(htmlMessage, @"<(.|\n)*?>", string.Empty),
                    HtmlBody = htmlMessage
                };

                email.Subject = subject;
                email.Body = builder.ToMessageBody();

                _logger.LogInformation("Sending email... [{0}] [{1}] [{2}]", recipient, subject, htmlMessage);

                var smtpClient = new MailKit.Net.Smtp.SmtpClient();
                if (_appSettings.Smtp.ValidateServerCertificate == false)
                {
                    smtpClient.ServerCertificateValidationCallback = (s, c, h, e) => true;
                }
                await smtpClient.ConnectAsync(server, port, useSsl);
                await smtpClient.AuthenticateAsync(username, pass);
                await smtpClient.SendAsync(email);

                _logger.LogInformation("The email sent successfully.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "The email can't sent");
            }
        }

        static void Disable_CertificateValidation()
        {
            // Disabling certificate validation can expose you to a man-in-the-middle attack
            // which may allow your encrypted message to be read by an attacker
            // https://stackoverflow.com/a/14907718/740639
            ServicePointManager.ServerCertificateValidationCallback =
                delegate (
                    object s,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors
                )
                {
                    return true;
                };
        }

    }
    ///// <summary>
    // /// provides methods to send email via smtp direct to mail server
    // /// </summary>
    //public class SmtpDirect
    //{
    //    /// <summary>
    //    /// Get / Set the name of the SMTP mail server
    //    /// </summary>
    //    private static string SmtpServer = "outbound-mail.yourorg.com";
    //    private enum SMTPResponse : int
    //    {
    //        CONNECT_SUCCESS = 220,
    //        GENERIC_SUCCESS = 250,
    //        DATA_SUCCESS = 354,
    //        QUIT_SUCCESS = 221
    //    }
    //    public static bool Send(MailMessage message)
    //    {
    //        IPHostEntry IPhst = Dns.GetHostEntry(SmtpServer);
    //        IPEndPoint endPt = new IPEndPoint(IPhst.AddressList[0], 25);
    //        Socket s = new Socket(endPt.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
    //        s.Connect(endPt);

    //        if (!Check_Response(s, SMTPResponse.CONNECT_SUCCESS))
    //        {
    //            s.Close();
    //            return false;
    //        }

    //        Senddata(s, string.Format("HELO {0}\r\n", Dns.GetHostName()));
    //        if (!Check_Response(s, SMTPResponse.GENERIC_SUCCESS))
    //        {
    //            s.Close();
    //            return false;
    //        }

    //        Senddata(s, string.Format("MAIL From: {0}\r\n", message.From));
    //        if (!Check_Response(s, SMTPResponse.GENERIC_SUCCESS))
    //        {

    //            s.Close();
    //            return false;
    //        }

    //        string _To = message.To;
    //        string[] Tos = _To.Split(new char[] { ';' });
    //        foreach (string To in Tos)
    //        {
    //            Senddata(s, string.Format("RCPT TO: {0}\r\n", To));
    //            if (!Check_Response(s, SMTPResponse.GENERIC_SUCCESS))
    //            {
    //                s.Close();
    //                return false;
    //            }
    //        }

    //        if (message.CC != null)
    //        {
    //            Tos = message.CC.Split(new char[] { ';' });
    //            foreach (string To in Tos)
    //            {
    //                Senddata(s, string.Format("RCPT TO: {0}\r\n", To));
    //                if (!Check_Response(s, SMTPResponse.GENERIC_SUCCESS))
    //                {
    //                    s.Close();
    //                    return false;
    //                }
    //            }
    //        }

    //        StringBuilder Header = new StringBuilder();
    //        Header.Append("From: " + message.From + "\r\n");
    //        Tos = message.To.Split(new char[] { ';' });
    //        Header.Append("To: ");
    //        for (int i = 0; i < Tos.Length; i++)
    //        {
    //            Header.Append(i > 0 ? "," : "");
    //            Header.Append(Tos[i]);
    //        }
    //        Header.Append("\r\n");
    //        if (message.Cc != null)
    //        {
    //            Tos = message.Cc.Split(new char[] { ';' });
    //            Header.Append("Cc: ");
    //            for (int i = 0; i < Tos.Length; i++)
    //            {
    //                Header.Append(i > 0 ? "," : "");
    //                Header.Append(Tos[i]);
    //            }
    //            Header.Append("\r\n");
    //        }
    //        Header.Append("Date: ");
    //        Header.Append(DateTime.Now.ToString("ddd, d M y H:m:s z"));
    //        Header.Append("\r\n");
    //        Header.Append("Subject: " + message.Subject + "\r\n");
    //        Header.Append("X-Mailer: SMTPDirect v1\r\n");
    //        string MsgBody = message.Body;
    //        if (!MsgBody.EndsWith("\r\n"))
    //            MsgBody += "\r\n";
    //        if (message.Attachments.Count > 0)
    //        {
    //            Header.Append("MIME-Version: 1.0\r\n");
    //            Header.Append("Content-Type: multipart/mixed; boundary=unique-boundary-1\r\n");
    //            Header.Append("\r\n");
    //            Header.Append("This is a multi-part message in MIME format.\r\n");
    //            StringBuilder sb = new StringBuilder();
    //            sb.Append("--unique-boundary-1\r\n");
    //            sb.Append("Content-Type: text/plain\r\n");
    //            sb.Append("Content-Transfer-Encoding: 7Bit\r\n");
    //            sb.Append("\r\n");
    //            sb.Append(MsgBody + "\r\n");
    //            sb.Append("\r\n");

    //            foreach (object o in message.Attachments)
    //            {
    //                MailAttachment a = o as MailAttachment;
    //                byte[] binaryData;
    //                if (a != null)
    //                {
    //                    FileInfo f = new FileInfo(a.Filename);
    //                    sb.Append("--unique-boundary-1\r\n");
    //                    sb.Append("Content-Type: application/octet-stream; file=" + f.Name + "\r\n");
    //                    sb.Append("Content-Transfer-Encoding: base64\r\n");
    //                    sb.Append("Content-Disposition: attachment; filename=" + f.Name + "\r\n");
    //                    sb.Append("\r\n");
    //                    FileStream fs = f.OpenRead();
    //                    binaryData = new Byte[fs.Length];
    //                    long bytesRead = fs.Read(binaryData, 0, (int)fs.Length);
    //                    fs.Close();
    //                    string base64String = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);

    //                    for (int i = 0; i < base64String.Length;)
    //                    {
    //                        int nextchunk = 100;
    //                        if (base64String.Length - (i + nextchunk) < 0)
    //                            nextchunk = base64String.Length - i;
    //                        sb.Append(base64String.Substring(i, nextchunk));
    //                        sb.Append("\r\n");
    //                        i += nextchunk;
    //                    }
    //                    sb.Append("\r\n");
    //                }
    //            }
    //            MsgBody = sb.ToString();
    //        }

    //        Senddata(s, ("DATA\r\n"));
    //        if (!Check_Response(s, SMTPResponse.DATA_SUCCESS))
    //        {
    //            s.Close();
    //            return false;
    //        }
    //        Header.Append("\r\n");
    //        Header.Append(MsgBody);
    //        Header.Append(".\r\n");
    //        Header.Append("\r\n");
    //        Header.Append("\r\n");
    //        Senddata(s, Header.ToString());
    //        if (!Check_Response(s, SMTPResponse.GENERIC_SUCCESS))
    //        {
    //            s.Close();
    //            return false;
    //        }

    //        Senddata(s, "QUIT\r\n");
    //        Check_Response(s, SMTPResponse.QUIT_SUCCESS);
    //        s.Close();
    //        return true;
    //    }
    //    private static void Senddata(Socket s, string msg)
    //    {
    //        byte[] _msg = Encoding.ASCII.GetBytes(msg);
    //        s.Send(_msg, 0, _msg.Length, SocketFlags.None);
    //    }
    //    private static bool Check_Response(Socket s, SMTPResponse response_expected)
    //    {
    //        string sResponse;
    //        int response;
    //        byte[] bytes = new byte[1024];
    //        while (s.Available == 0)
    //        {
    //            System.Threading.Thread.Sleep(100);
    //        }

    //        s.Receive(bytes, 0, s.Available, SocketFlags.None);
    //        sResponse = Encoding.ASCII.GetString(bytes);
    //        response = Convert.ToInt32(sResponse.Substring(0, 3));
    //        if (response != (int)response_expected)
    //            return false;
    //        return true;
    //    }
    //}
}
