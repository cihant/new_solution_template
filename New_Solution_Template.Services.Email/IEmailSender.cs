using System.Net.Mail;
using System.Threading.Tasks;

namespace New_Solution_Template.Services.Email
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string recipient, string subject, string htmlMessage);
        Task SendEmailAsync(string recipient, string reciepentName, string subject, string htmlMessage);
        Task SendEmailAsync(MailAddress recipient, string subject, string htmlMessage);
     }
}
