using System;
using System.Collections.Generic;
using System.Net;

namespace New_Solution_Template.Models
{
    public class ResponseModel : ResponseModel<string>
    {
        public new static ResponseModel Create(string data, HttpStatusCode status = HttpStatusCode.OK)
        {
            var response = new ResponseModel()
            {
                Status = status,
                Data = data
            };

            return response;
        }

    }
    public class ResponseModel<T>
    {
        public bool Success => this.Status == HttpStatusCode.OK;
        public HttpStatusCode Status { get; set; }
        public int CodeInt
        {
            get { return (int)Status; }
            set { Status = (HttpStatusCode)value; }
        }
        public T Data { get; set; }
        public ResponseErrorModel Error { get; set; }

        public static ResponseModel<T> Create(T data, HttpStatusCode status = HttpStatusCode.OK)
        {
            var response = new ResponseModel<T>()
            {
                Status = status,
                Data = data
            };

            return response;
        }
        public static ResponseModel<Exception> Create(Exception ex, int code, string message, HttpStatusCode status = HttpStatusCode.InternalServerError)
        {
            var response = new ResponseModel<Exception>()
            {
                Status = status,
#if DEBUG
                Data = ex,
#endif
                Error = new ResponseErrorModel()
                {
                    Code = code,
                    Message = message
                }
            };

            return response;
        }
    }

    public class ResponseErrorModel
    {
        public int Code { get; set; }
        public string Key => this.Code.ToString();
        public string Message { get; set; }
        public List<string> Validations { get; set; }
    }
}
