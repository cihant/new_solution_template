using Mapster;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Oreo.Core.Extensions;

namespace New_Solution_Template.Models
{
    public static class Mapper
    {
        public static void MapperConfig()
        {
            TypeAdapterConfig.GlobalSettings.Scan(Assembly.GetEntryAssembly());

            JsonSerializerSettings jss = new JsonSerializerSettings();
            jss.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            TypeAdapterConfig<DateTime, string>
                    .NewConfig()
                    .MapWith(src => src.ToLocalDateTimeString())
                    ;
        }
    }
}
