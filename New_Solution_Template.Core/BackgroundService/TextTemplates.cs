using System;
using System.Collections.Generic;
using System.Text;

namespace New_Solution_Template.Core.BackgroundService
{
   public class TextTemplates
    {
        public string Subject { get; set; } = "";
        public string BodyTemplate { get; set; } = "";
    }
}
