using System;
using System.Collections.Generic;
using System.Text;

namespace New_Solution_Template.Core.BackgroundService
{
    public class Settings 
    {
        public string CronExp { get; set; }
        public decimal Maximum { get; set; } = decimal.MaxValue;
        public decimal Minimum { get; set; } = decimal.MinValue;
        public int InMinutes { get; set; }
        public string SubscriberType { get; set; }
        public string[] EmailRecipients { get; set; }
        public string[] SlackWebhooks { get; set; }
        public bool FollowAlertStatus { get; set; }
        public int AlertReminderMinutes { get; set; }
        public bool Enabled { get; set; }
        public TextTemplates AlertTexts { get; set; }
        public TextTemplates AlertClearedTexts { get; set; }
    }
}
