using New_Solution_Template.Core.BackgroundService;
using System;
using System.Collections.Generic;
using System.Text;

namespace New_Solution_Template.Core
{
    public class AppSettings
    {
        public GoogleOptionsSettings GoogleOptions { get; set; }
        public string DBConnectionString { get; set; }
        public BackgroundServicesSettings BackgroundServices { get; set; }
        public SmtpSettings Smtp { get; set; }
        public string SiteLocale { get; set; }
    }

    public class SmtpSettings
    {
        public string Server { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Pass { get; set; }
        public string From { get; set; }
        public string FromName { get; set; }
        public bool UseSsl { get; set; }
        public bool ValidateServerCertificate { get; set; }
    }

    public class BackgroundServicesSettings
    {
        public Settings PaidSubscriberCounts { get; set; }
        public Settings FreeSubscriberCounts { get; set; }
        public Settings IncomingMessageCounts { get; set; }
        public Settings MessageQueueDecreaseChecker { get; set; }
        public Settings MonthlyCollectedMoney { get; set; }
        public Settings MonthlyMoneyCollectedSubscriber { get; set; }
        
    }
    public class GoogleOptionsSettings     {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
