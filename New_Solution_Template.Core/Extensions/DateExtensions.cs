﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oreo.Core.Extensions
{
    public static class DateExtensions
    {
        public static DateTime ToUtcDateTime(this DateTime target)
        {
            DateTime localDate = System.DateTime.SpecifyKind(target, DateTimeKind.Local);
            DateTime utcDate = localDate.ToUniversalTime();
            return utcDate;
        }

        public static DateTime ToLocalDateTime(this DateTime s)
        {
            DateTime utcDate = System.DateTime.SpecifyKind(s, DateTimeKind.Utc);
            DateTime localDate = utcDate.ToLocalTime();
            return localDate;
        }

        public static string ToLocalDateTimeString(this DateTime s, System.Globalization.CultureInfo culture)
        {
            DateTime utcDate = System.DateTime.SpecifyKind(s, DateTimeKind.Utc);
            DateTime localDate = utcDate.ToLocalTime();

            string localDateStr = localDate.ToString("g", culture);
            return localDateStr;
        }

        public static string ToLocalDateTimeString(this DateTime s, string cultureName)
        {
            return s.ToLocalDateTimeString(CultureInfo.CreateSpecificCulture(cultureName));
        }

        public static string ToLocalDateTimeString(this DateTime s)
        {
            return s.ToLocalDateTimeString(CultureInfo.CurrentCulture);
        }

        public static string ToLocalDateString(this DateTime s, CultureInfo culture)
        {
            DateTime utcDate = System.DateTime.SpecifyKind(s, DateTimeKind.Utc);
            DateTime localDate = utcDate.ToLocalTime();

            string localDateStr = localDate.ToString("d", culture);
            return localDateStr;
        }

        public static string ToLocalDateString(this DateTime s, string cultureName)
        {
            return s.ToLocalDateString(CultureInfo.CreateSpecificCulture(cultureName));
        }

        public static string ToLocalDateString(this DateTime s)
        {
            return s.ToLocalDateString(CultureInfo.CurrentCulture);
        }

        public static DateTime FromUnixTime(this long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        public static long ToUnixTime(this DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalSeconds);
        }
    }
}
