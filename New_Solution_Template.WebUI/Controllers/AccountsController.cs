using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataTablesParser;
//using New_Solution_Template.Data.Entity;
using Microsoft.AspNetCore.Mvc;
using New_Solution_Template.Services;
using New_Solution_Template.Data;
using Mapster;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace New_Solution_Template.WebUI.Controllers
{
    public class AccountsController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly ILogger<AccountService> _logger;

        public AccountsController(IAccountService accountService,
            ILogger<AccountService> logger
            )
        {
            _accountService = accountService;
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
   [HttpPost]
        public IActionResult GetDatatableSource()
        {
            var parser = new Parser<ApplicationUser>(Request.Form, _accountService.GetAll());
            return Json(parser.Parse());
        }


        [HttpGet]
        public async Task<ActionResult> GetUsers(string userReferenceIds, string search = "", int page = 1)
        {
            try
            {
                int dataCountPerPage = 10;

                //find
                IQueryable<ApplicationUser> query = null;

                query = _accountService.GetActiveUsers();

                dynamic data = new dynamic[] { };

                if (query != null)
                {
                    query = query.OrderByDescending(a => a.FirstName).ThenByDescending(a => a.EmploymentDate);

                   if (!string.IsNullOrWhiteSpace(search))
                    {
                        query = query.Where(a => a.UserName.Contains(search) || a.FirstName.Contains(search) || a.LastName.Contains(search));
                    }

                    //skip-take
                    query = query.Skip((page - 1) * dataCountPerPage).Take(dataCountPerPage);

                    //map result
                    data = (await query.ToListAsync()).Select(a => new
                    {
                        text = a.FullName,
                        date = a.EmploymentDate,
                        id = a.Id
                    });
                }

                return Json(data);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "GetPushActionReferences could not be retrived data. search:{search} page:{page}", search, page);
            }

            return Json(new {  });
        }
    }
}
