﻿$(function () {


    /* # Bootstrap Plugins
    ================================================== */

    //===== Add fadeIn animation to dropdown =====//

    $('.dropdown, .btn-group').on('show.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeIn(100);
    });


    //===== Add fadeOut animation to dropdown =====//

    $('.dropdown, .btn-group').on('hide.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeOut(100);
    });

    //===== Tooltip =====//

    $('.tip').tooltip();

    //===== Popover =====//

    $("[data-toggle=popover]").popover().click(function (e) {
        e.preventDefault();
    });



    /* # Interface Related Plugins
    ================================================== */


    //===== Collapsible navigation =====//

    $('.expand').collapsible({
        defaultOpen: 'second-level,third-level',
        cssOpen: 'level-opened',
        cssClose: 'level-closed',
        speed: 150
    });





    /* # Default Layout Options
    ================================================== */

    //===== Hiding sidebar =====//

    $('.sidebar-toggle').click(function () {
        $('.page-container').toggleClass('hidden-sidebar');
    });


    /* # Select2 dropdowns 
================================================== */


    //===== Datatable select =====//

    $(".dataTables_length select").select2({
        minimumResultsForSearch: "-1"
    });


    //===== Default select =====//

    $(".select").select2({
        minimumResultsForSearch: "-1",
        width: 200
    });


    //===== Liquid select =====//

    $(".select-liquid").select2({
        minimumResultsForSearch: "-1",
        width: "off"
    });


    //===== Full width select =====//

    $(".select-full").select2({
        minimumResultsForSearch: "-1",
        width: "100%"
    });


    //===== Select with filter input =====//

    $(".select-search").select2({
        width: 200
    });
    $(".select-search-full").select2({
        width: "100%"
    });


    //===== Multiple select =====//

    $(".select-multiple").select2({
        width: "100%"
    });


    //===== Loading data select =====//

    $("#loading-data").select2({
        placeholder: "Enter at least 1 character",
        allowClear: true,
        minimumInputLength: 1,
        query: function (query) {
            var data = { results: [] }, i, j, s;
            for (i = 1; i < 5; i++) {
                s = "";
                for (j = 0; j < i; j++) { s = s + query.term; }
                data.results.push({ id: query.term + i, text: s });
            }
            query.callback(data);
        }
    });


    //===== Select with maximum =====//

    $(".maximum-select").select2({
        maximumSelectionSize: 3,
        width: "100%"
    });


    //===== Allow clear results select =====//

    $(".clear-results").select2({
        placeholder: "Select a State",
        allowClear: true,
        width: 200
    });


    //===== Select with minimum =====//

    $(".minimum-select").select2({
        minimumInputLength: 2,
        width: 200
    });


    //===== Multiple select with minimum =====//

    $(".minimum-multiple-select").select2({
        minimumInputLength: 2,
        width: "100%"
    });


    //===== Disabled select =====//

    $(".select-disabled").select2(
        "enable", false
    );




    /* # Form Related Plugins
    ================================================== */

    //===== Form elements styling =====//

    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice',
        selectAutoWidth: false
    });


    //===== Elastic textarea =====//

    $('.elastic').autosize();

});

/* confirm - alert helpers */

function ldsAlertError(message) {
    if (!message) {
        message = 'İşlem gerçekleştirilemedi, bir süre sonra tekrar deneyiniz.';
    }

    $.alert({
        icon: 'fa fa-exclamation f22',
        closeIcon: true,
        closeIconClass: 'fa fa-close',
        backgroundDismiss: true,
        escapeKey: true,
        title: Resources.Error,
        content: message,
        buttons: {
            ok: { text: Resources.OK, action: function () { } }
        }
    });
}

function ldsConfirm(iconClass, title, message, successAction, failAction) {

    $.confirm({
        icon: 'fa f22 ' + iconClass,
        closeIcon: true,
        closeIconClass: 'fa fa-close',
        backgroundDismiss: true,
        escapeKey: true,
        title: title,
        content: message,
        buttons: {
            confirm: {
                text: Resources.Continue,
                action: successAction
            },
            cancel: {
                text: Resources.Cancel,
                action: failAction
            }
        }
    });
}
function ldsConfirmDelete(successAction) {
    ldsConfirm('fa-trash-o', 'Siliniyor', 'Kayıt silinecek, onaylıyor musunuz?', successAction, null);
}
function ldsConfirmActivePassive(selectedCheckbox, successAction) {

    var isChecked = selectedCheckbox.is(':checked');

    var title = Resources.GettingPassive;
    var content = Resources.GettingPassiveConfirmation;
    if (isChecked) {
        title = Resources.GettingActive;
        content = Resources.GettingActiveConfirmation;
    }

    $.confirm({
        icon: 'fa fa-warning f22',
        closeIcon: true,
        closeIconClass: 'fa fa-close',
        backgroundDismiss: false,
        escapeKey: true,
        title: title,
        content: content,
        buttons: {
            confirm: {
                text: Resources.Continue,
                action: successAction
            },
            cancel: {
                text: Resources.Cancel,
                action: function () {
                    selectedCheckbox.prop('checked', !isChecked);
                    $.uniform.update();
                }
            }
        }
    });

}

//--

/* jGrowl/Notification helpers */
function ldsGrowlDeleted() {

    $.jGrowl('Kayıt başarılı bir şekilde silindi.', { sticky: false, theme: 'growl-success', header: 'İşlem başarılı!' });
}
function ldsGrowlUpdated() {

    $.jGrowl('Kayıt başarılı bir şekilde güncellendi.', { sticky: false, theme: 'growl-success', header: 'İşlem başarılı!' });
}
//-

/* string prototype functions */
String.prototype.format = function () {
    var s = this;
    for (var i = 0; i < arguments.length; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i]);
    }

    return s;
}

String.prototype.endsWith = function (suffix) {
    return (this.substr(this.length - suffix.length) === suffix);
}

String.prototype.startsWith = function (prefix) {
    return (this.substr(0, prefix.length) === prefix);
}
//--

/* Form submit buttons - loading state */
jQuery(function ($) {
    $("form").submit(function (e) {
        //button changing loading state
        var clickedButton = $("button[type=submit][data-loading-text][clicked=true]");
        clickedButton.button('loading');

        //if form is invalid, button reset loading state
        if (!$(this).valid()) {
            clickedButton.button('reset');
        }
    });

    $("button[type=submit]").click(function () {
        $("button[type=submit]", $(this).parents("form")).removeAttr("clicked");
        $(this).attr("clicked", "true");
    });
});
//--

function getQueryStringParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function decodeHtmlEntity(str) {
    return str.replace(/&#(\d+);/g, function (match, dec) {
        return String.fromCharCode(dec);
    });
};
function encodeHtmlEntity(str) {
    return str.replace(/[\u00A0-\u9999\<\>\&\'\"\\\/]/gim, function (c) {
        return '&#' + c.charCodeAt(0) + ';';
    });
}

function renderBoolean(data, type, row) {
    if (data === true) {
        return "<i class='fa fa-check' aria-hidden='true'></i>";
    }
    return "";
}



$('.datepicker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
        format: 'DD.MM.YYYY',
        applyLabel: 'Uygula',
        cancelLabel: 'Vazgeç'
    }
});

$('.datetimepicker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    timePicker: true,
    timePickerIncrement: 30,
    timePicker24Hour: true,
    locale: {
        format: 'DD.MM.YYYY HH:mm',
        applyLabel: 'Uygula',
        cancelLabel: 'Vazgeç'
    }
});