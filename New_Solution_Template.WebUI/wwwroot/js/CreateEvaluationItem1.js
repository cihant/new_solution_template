﻿$(document).on("click", "#btnEvaluationItemsCreate", function () {

    var itemName = $("#txtEvaluationItemName").val();
    var itemTitle = $("#txtEvaluationItemTitle").val();
    var itemShort = $("#txtEvaluationItemShort").val();
    var itemDescription = $("#txtEvaluationItemDescription").val();
    var emp = {
        "itemName": itemName,
        "itemTitle": itemTitle,
        "itemShort": itemShort,
        "itemDescription": itemDescription
    };

    $.ajax({
        method: "POST",
        url: "/api/EvaluationItem/EvaluationItemCreate",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(emp),
        success: function (data) {
            window.location.reload();
        },
        error: function (data) {
            alert(data);
            alert("Bir Hata Oluştu");
        }
    });
});