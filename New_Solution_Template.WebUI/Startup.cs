using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using New_Solution_Template.Core;
using New_Solution_Template.Services;
using New_Solution_Template.Core.Caching;
using New_Solution_Template.Data;
using New_Solution_Template.Services.Email;
using Microsoft.AspNetCore.Authentication.OAuth;
using Newtonsoft.Json.Serialization;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;

namespace New_Solution_Template.WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            New_Solution_Template.Models.Mapper.MapperConfig();
            services.Configure<AppSettings>(Configuration);

            var appConfig = new AppSettings();
            Configuration.GetSection("AppSettings").Bind(appConfig);
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings").Bind);
            services.AddSingleton(appConfig);

            services.AddEntityFrameworkNpgsql().AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(appConfig.DBConnectionString));
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddTransient<Data.Initializers.DBInitializer>();

            services.AddSingleton<IStaticCacheManager, MemoryCacheManager>();

            services.AddTransient<IAccountService, AccountService>();
        
            services.AddAuthentication().AddGoogle(googleOptions =>
            {
                googleOptions.ClientId = appConfig.GoogleOptions.ClientId;
                googleOptions.ClientSecret = appConfig.GoogleOptions.ClientSecret;
                googleOptions.Events = new OAuthEvents
                {
                    OnRedirectToAuthorizationEndpoint = context =>
                    {
                        context.Response.Redirect(context.RedirectUri + "&hd=" + System.Net.WebUtility.UrlEncode("loodos.com"));

                        return Task.CompletedTask;
                    },
                    OnCreatingTicket = context =>
                    {
                        string domain = context.User.Value<string>("email");
                        if (domain.EndsWith("loodos.com") == false)
                            throw new Exception("You must sign in with a loodos.com email address");

                        return Task.CompletedTask;
                    }
                };
            });

            services.AddScoped<IEmailSender, EmailSender>();

            services.AddMvc()
                    .AddJsonOptions(options =>
                    {
                        options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                    })
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);


            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddDefaultUI(UIFramework.Bootstrap4)
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, Data.Initializers.DBInitializer dBInitializer, IOptions<AppSettings> appSettings)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            SetLocale(app , appSettings.Value.SiteLocale);

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //dBInitializer.Seed().Wait();
        }

        private void SetLocale(IApplicationBuilder app, string siteLocale)
        {
            RequestLocalizationOptions localizationOptions = new RequestLocalizationOptions
            {
                SupportedCultures = new List<CultureInfo> { new CultureInfo(siteLocale) },
                SupportedUICultures = new List<CultureInfo> { new CultureInfo(siteLocale) },
                DefaultRequestCulture = new RequestCulture(siteLocale)
            };
            app.UseRequestLocalization(localizationOptions);
        }
    }
}
