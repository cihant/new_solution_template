using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace New_Solution_Template.Data
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public DateTime EmploymentDate { get; set; }
        public DateTime UnemploymentDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }


        public string ParentId { get; set; }
        public virtual ApplicationUser Parent { get; set; }

        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }

    }
}
