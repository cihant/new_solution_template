using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace New_Solution_Template.Data
{
    public class ApplicationRole : IdentityRole
    {
        public const string SystemAdmin = "SystemAdmin";
        public const string User = "User";


        public readonly static string[] DefaultRoles = new string[] { SystemAdmin, User };

    }
}
