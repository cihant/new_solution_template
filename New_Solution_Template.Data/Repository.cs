using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using New_Solution_Template.Core.Caching;

namespace New_Solution_Template.Data
{

    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        protected IStaticCacheManager _cacheManager;
        protected readonly ApplicationDbContext context;
        //  protected readonly ILogger _logger;

        protected DbSet<T> entities;

        public virtual string CACHE_PATTERN_KEY { get; }

        public Repository(ApplicationDbContext context,
            IStaticCacheManager cacheManager
            //  ILogger logger
            )
        {
            this.context = context;
            this._cacheManager = cacheManager;
            //  _logger = logger;
            entities = context.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            return entities.AsQueryable();
        }

        public virtual async Task<ICollection<T>> GetAllAsync()
        {

            return await entities.ToListAsync();
        }

        public virtual T Get(int id)
        {
            if (id == 0)
                return null;

            string key = string.Format(CACHE_PATTERN_KEY, id);
            return _cacheManager.Get(key, () => entities.Find(id));

            //return entities.Find(id);
        }

        public virtual async Task<T> GetAsync(int id)
        {
            if (id == 0)
                return null;

            string key = string.Format(CACHE_PATTERN_KEY, id);
            return await _cacheManager.Get(key, async () => await entities.FindAsync(id));

            //            return await entities.FindAsync(id);
        }

        public virtual T Add(T t)
        {
            if (t is AuditableEntity)
            {
                var tt = t as AuditableEntity;
                tt.CreateDate = DateTime.Now;
                tt.ModifiedDate = DateTime.Now;
            }
            entities.Add(t);

            //clear cache
            // _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);

            SaveChanges();
            return t;
        }

        public virtual async Task<T> AddAsync(T t)
        {
            if (t is AuditableEntity)
            {
                var tt = t as AuditableEntity;
                tt.CreateDate = DateTime.Now;
                tt.ModifiedDate = DateTime.Now;
            }
            await entities.AddAsync(t);
            await SaveChangesAsync();

            //clear cache
            //  _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);

            return t;

        }

        public virtual T Find(Expression<Func<T, bool>> match)
        {
            return entities.SingleOrDefault(match);
        }

        public virtual async Task<T> FindAsync(Expression<Func<T, bool>> match)
        {
            return await entities.SingleOrDefaultAsync(match);
        }

        public ICollection<T> FindAll(Expression<Func<T, bool>> match)
        {
            return entities.Where(match).ToList();
        }

        public async Task<ICollection<T>> FindAllAsync(Expression<Func<T, bool>> match)
        {
            return await entities.Where(match).ToListAsync();
        }

        public virtual void Delete(T entity)
        {
            if (entity is AuditableEntity)
            {
                var t = entity as AuditableEntity;
                t.IsActive = false;
                t.IsDeleted = true;
                t.ModifiedDate = DateTime.Now;
                Update(entity, entity.Id);
            }
            else
            {
                entities.Remove(entity);
                SaveChanges();
            }
            //TODO: Add log record to db with userid
        }

        public virtual async Task DeleteAsync(T entity)
        {
            if (entity is AuditableEntity)
            {
                var t = entity as AuditableEntity;
                t.IsActive = false;
                t.IsDeleted = true;
                t.ModifiedDate = DateTime.Now;
                await UpdateAsync(entity, entity.Id);
            }
            else
            {
                throw new NotSupportedException("Hard delete not supported");
            }
            //TODO: Add log record to db with userid
        }

        public virtual T Update(T t, params object[] key)
        {
            if (t == null)
                return null;
            T exist = entities.Find(key);
            if (exist != null)
            {
                if (t is AuditableEntity)
                {
                    var tt = t as AuditableEntity;
                    tt.ModifiedDate = DateTime.Now;
                }
                //TODO: Add log record to db with userid
                context.Entry(exist).CurrentValues.SetValues(t);
                SaveChanges();

                //clear cache
                //  _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
            }

            return exist;
        }

        public virtual async Task<T> UpdateAsync(T t, params object[] key)
        {
            if (t == null)
                return null;
            T exist = await entities.FindAsync(key);
            if (exist != null)
            {
                if (t is AuditableEntity)
                {
                    var tt = t as AuditableEntity;
                    tt.ModifiedDate = DateTime.Now;
                }
                //TODO: Add log record to db with userid
                context.Entry(exist).CurrentValues.SetValues(t);
                await context.SaveChangesAsync();

                //clear cache
                // _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
            }
            return exist;
        }

        public int Count()
        {
            return entities.Count();
        }

        public async Task<int> CountAsync()
        {
            return await entities.CountAsync();
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await context.SaveChangesAsync();
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = entities.Where(predicate);
            return query;
        }

        public virtual async Task<ICollection<T>> FindByAsync(Expression<Func<T, bool>> predicate)
        {
            return await entities.Where(predicate).ToListAsync();
        }

        public IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {

            IQueryable<T> queryable = GetAll();
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {

                queryable = queryable.Include<T, object>(includeProperty);
            }

            return queryable;
        }

        public bool IsExists(params object[] key)
        {
            // return entities.Any(e => e.Keys == key);
            return true;
        }

        public void ChangeState(T t, EntityState entityState)
        {
            context.Entry(t).State = entityState;
        }


        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
