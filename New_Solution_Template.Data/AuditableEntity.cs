using System;

namespace New_Solution_Template.Data
{
    public abstract class AuditableEntity : BaseEntity
    {
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
