using New_Solution_Template.Core;
using New_Solution_Template.Core.Caching;
using New_Solution_Template.Data;
//using New_Solution_Template.Data.Entity;
using New_Solution_Template.Models;
using New_Solution_Template.Services.Email;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace New_Solution_Template.Services
{
    public class AccountService : IAccountService
    {
        private readonly IEmailSender _emailSender;
        private readonly IOptions<AppSettings> _options;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<AccountService> _logger;

        public AccountService(
            UserManager<ApplicationUser> userManager,
            ILogger<AccountService> logger,
            IEmailSender emailSender, IOptions<AppSettings> options)
        {
            _userManager = userManager;
            _logger = logger;
            _emailSender = emailSender;
            _options = options;
        }

        public async Task<IdentityResult> CreateAsync(string userEmail, string firstName, string lastName, ExternalLoginInfo externalLoginInfo)
        {
            ApplicationUser user = new ApplicationUser { UserName = userEmail, Email = userEmail, FirstName = firstName, LastName = lastName };
            var result = await _userManager.CreateAsync(user);
            if (result.Succeeded)
            {
                result = await _userManager.AddLoginAsync(user, externalLoginInfo);
                if (result.Succeeded)
                {
                    if (GetActiveUsers().Count() == 0)
                    {
                        await _userManager.AddToRoleAsync(user, ApplicationRole.SystemAdmin);
                    }
                    await _userManager.AddToRoleAsync(user, ApplicationRole.User);

                    _logger.LogInformation("User Olusturuldu", user.Email);
                    await _emailSender.SendEmailAsync("cihan.topcu@loodos.com", "Yeni Kayit Olusturuldu", userEmail + " mailli kullanici sisteme kayit oldu");
                }
            }
            else
            {
                _logger.LogInformation("User olusturulurken hata olustu", user.Email);

            }
            return result;
        }

        public async Task<ResponseModel> DeleteUserAsync(string userId)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                _logger.LogInformation("[AS_DU1] Kullanici bulunamadi", userId);
                return await Task.FromResult<ResponseModel>(ResponseModel.Create("Hata olustu", System.Net.HttpStatusCode.InternalServerError));
            }

            user.IsActive = false;
            user.IsDeleted = false;
            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                _logger.LogInformation("[AS_DU2] Silme islemi basariyla gerçeklesti", user.Email);
                await _emailSender.SendEmailAsync(_options.Value.Smtp.From, "Silme islemi basariyla gerçeklesti", user.Email);
                return ResponseModel.Create("Kayit basarili");
            }
            else
            {
                _logger.LogInformation("[AS_DU3] User olusturulurken hata olustu", user.Email);
                var response = ResponseModel.Create("Hata olustu", System.Net.HttpStatusCode.InternalServerError);
                response.Error = new ResponseErrorModel
                {
                    Validations = result.Errors.Select(ie => ie.Description).ToList()
                };
                return response;
            }
        }

        public async Task<ApplicationUser> GetAsync(string userId)
        {
            return await _userManager.FindByIdAsync(userId);
        }
        public IQueryable<ApplicationUser> GetActiveUsers()
        {
            return _userManager.Users.Where(x => x.IsActive && !x.IsDeleted).AsQueryable();
        }
        public IQueryable<ApplicationUser> GetAll()
        {
            return _userManager.Users.AsQueryable();
        }
        //public EvaluationFormWaitingResponse FindEvaluationFormUserManagerWaiting(UserManagerIdRequest userManagerIdRequest)
        //{
        //    string a;
        //    Data.ApplicationUser user = base.Find(x => x.ParentId == userManagerIdRequest.managerId && x.Id == userManagerIdRequest.userId);
        //    EvaluationFormWaitingResponse evaluationFormWaitingResponse = new EvaluationFormWaitingResponse();
        //    if (user != null)
        //    {
        //        evaluationFormWaitingResponse.userName = user.Name + " " + user.Surname;
        //    }
        //    a = "ff";
        //    return evaluationFormWaitingResponse;
        //}

        //public EvaluationFormWaitingResponse FindEvaluationFormWaiting(IdRequest idRequest)
        //{
        //    string userName = Find(x => x.Id == idRequest.id).Name + " " + Find(x => x.Id == idRequest.id).Surname;
        //    EvaluationFormWaitingResponse evaluationFormWaitingResponse = new EvaluationFormWaitingResponse();
        //    evaluationFormWaitingResponse.userName = userName;
        //    return evaluationFormWaitingResponse;
        //}

        //public UserResponse FindManager(IdRequest idRequest)
        //{
        //    Data.ApplicationUser user = base.Find(x => x.Id == idRequest.id);
        //    UserResponse userResponse = new UserResponse();
        //    userResponse.department = user.Department;
        //    userResponse.email = user.EMail;
        //    userResponse.entryDate = user.EntryDate.ToString("dd/MM/yyy");
        //    userResponse.id = user.Id;
        //    userResponse.name = user.Name;
        //    userResponse.surname = user.Surname;
        //    return userResponse;
        //}

        //public UserResponse FindUser(IdRequest idRequest)
        //{
        //    Data.ApplicationUser user = base.Find(x => x.Id == idRequest.id);
        //    List<Data.ApplicationUser> users = new List<Data.ApplicationUser>();
        //    users.Add(Find(x => x.Id == user.ParentId && x.IsDeleted == false));
        //    UserResponse userResponse = new UserResponse();
        //    userResponse.department = user.Department;
        //    userResponse.email = user.EMail;
        //    userResponse.entryDate = user.EntryDate.ToString("dd/MM/yyy");
        //    userResponse.id = user.Id;
        //    userResponse.manager = user.ParentId.ToString();
        //    userResponse.name = user.Name;
        //    userResponse.surname = user.Surname;
        //    userResponse.managerList = users.Union(FindAll(x => x.Discriminator == "Manager" && x.Id != user.ParentId && x.IsDeleted == false).ToList()).ToList();
        //    return userResponse;
        //}

        //public List<UserResponse> ListManager()
        //{
        //    List<UserResponse> userListResponses = new List<UserResponse>();
        //    List<Data.ApplicationUser> users = FindAll(x => x.Discriminator == "Manager" && x.IsDeleted == false).OrderByDescending(x => x.Id).ToList();
        //    foreach (var item in users)
        //    {

        //        userListResponses.Add(new UserResponse()
        //        {
        //            id = item.Id,
        //            department = item.Department,
        //            entryDate = item.EntryDate.ToString("dd/MM/yyyy"),
        //            name = item.Name,
        //            surname = item.Surname,
        //            email = item.EMail
        //        });

        //    }

        //    return userListResponses;
        //}

        //public List<UserResponse> ListUser()
        //{
        //    List<UserResponse> userListResponses = new List<UserResponse>();
        //    // List<User> users = FindAll(x => x.Discriminator == "User" && x.IsDeleted == false && x.Name!=null).OrderByDescending(x => x.Id).ToList();
        //    List<Data.ApplicationUser> users = FindAll(x => x.IsDeleted == false && x.Name != null).OrderByDescending(x => x.Id).ToList();
        //    foreach (var item in users)
        //    {
        //        string managerName;
        //        if (item.ParentId != 0)
        //            managerName = Find(x => x.Id == item.ParentId).Name + " " + Find(x => x.Id == item.ParentId).Surname;
        //        else
        //            managerName = "Yönetici";



        //        userListResponses.Add(new UserResponse()
        //        {
        //            id = item.Id,
        //            department = item.Department,
        //            entryDate = item.EntryDate.ToString("dd/MM/yyyy"),
        //            manager = managerName,
        //            name = item.Name,
        //            surname = item.Surname,
        //            email = item.EMail
        //        });

        //    }

        //    return userListResponses;
        //}

        //public string ManagerMail(IdRequest idRequest)
        //{
        //    int managerId = Find(x => x.Id == idRequest.id).ParentId;

        //    return Find(x => x.Id == managerId).EMail;

        //}

        public async Task<IdentityResult> UpdateAsync(ApplicationUser user)
        {
            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                _logger.LogInformation("[AS_UA1] User Güncellendi", user.Email);
                await _emailSender.SendEmailAsync("cihan.topcu@loodos.com", "Kullanici Güncellendi", user.Email + " mailli kullanici güncellendi");
            }
            else
            {
                _logger.LogInformation("[AS_UA2] User güncellenirken hata olustu", user.Email);

            }
            return result;
        }
    }
}
