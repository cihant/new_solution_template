using New_Solution_Template.Data;
using New_Solution_Template.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace New_Solution_Template.Services
{
    public interface IAccountService
    {
        //ResponseModel CreateUser(UserEmailRequest userEmailRequest);
        //List<UserResponse> ListUser();
        //List<UserResponse> ListManager();
        //UserResponse FindUser(IdRequest idRequest);
        //UserResponse FindManager(IdRequest idRequest);
        //ResponseModel UpdateUser(UserRequest userRequest);
        //ResponseModel DeleteUser(IdRequest idRequest);

        //string ManagerMail(IdRequest idRequest);


        Task<ResponseModel> DeleteUserAsync(string userId);
        Task<IdentityResult> CreateAsync(string userEmail, string firstName, string lastName, ExternalLoginInfo externalLoginInfo);
        Task<IdentityResult> UpdateAsync(ApplicationUser user);
        Task<ApplicationUser> GetAsync(string userId);
        IQueryable<ApplicationUser> GetActiveUsers();
        IQueryable<ApplicationUser> GetAll();


        //EvaluationFormWaitingResponse FindEvaluationFormWaiting(IdRequest idRequest);
        //EvaluationFormWaitingResponse FindEvaluationFormUserManagerWaiting(UserManagerIdRequest userManagerIdRequest);
    }
}
